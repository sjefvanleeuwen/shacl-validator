// Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import {describe, Done} from 'mocha'
import * as request from 'request';
import { expect } from 'chai'

let rf = require('read-file');

let base:string = process.env.VALIDATOR_BASE || "http://localhost:8080";
let url:string = base + "/api/validate";

function validate_files(data:string, data_type:string, shape:string, shape_type:string, cb:Function) {
    let form_data = {
        'data_type': data_type,
        'shape_type': shape_type
    };

    rf(shape, 'utf8', (err, shape) => {
        expect(err).to.be.a('null');
        form_data['shape'] = shape;

        rf(data, 'utf8', (err, data) => {
            expect(err).to.be.a('null');

            form_data['data'] = data;
            request.post(url, {form: form_data}, (error: any, response: request.Response, data: any) =>{
                expect(error).to.be.a('null');

                // handle response
                expect(response.statusCode).to.equal(200);

                data = JSON.parse(data);
                expect(data["conforms"]).to.equal(false);
                expect(data["simple"].length).to.equal(2);
                cb();
            });
        });
    });
}

function validate_remote_data(data:string, data_type:string, shape:string, shape_type:string, cb:Function) {
    let form_data = {
        'data_type': data_type,
        'data': data,
        'shape_type': shape_type
    };

    rf(shape, 'utf8', (err, shape) => {
        expect(err).to.be.a('null');
        form_data['shape'] = shape;

        request.post(url, {form: form_data}, (error: any, response: request.Response, data: any) =>{
            expect(error).to.be.a('null');

            // handle response
            expect(response.statusCode).to.equal(200);

            data = JSON.parse(data);
            expect(data["conforms"]).to.equal(false);
            expect(data["simple"].length).to.equal(2);
            cb();
        });
    });
}

function validate_remote_shape(data:string, data_type:string, shape:string, shape_type:string, cb:Function) {
    let form_data = {
        'data_type': data_type,
        'shape_type': shape_type,
        'shape': shape
    };

    rf(data, 'utf8', (err, data) => {
        expect(err).to.be.a('null');

        form_data['data'] = data;
        request.post(url, {form: form_data}, (error: any, response: request.Response, data: any) =>{
            expect(error).to.be.a('null');

            // handle response
            expect(response.statusCode).to.equal(200);

            data = JSON.parse(data);
            expect(data["conforms"]).to.equal(false);
            expect(data["simple"].length).to.equal(2);
            cb();
        });
    });
}

function validate_remote(data:string, data_type:string, shape:string, shape_type:string, cb:Function) {
    let form_data = {
        'data_type': data_type,
        'data': data,
        'shape_type': shape_type,
        'shape': shape
    };

    request.post(url, {form: form_data}, (error: any, response: request.Response, data: any) =>{
        expect(error).to.be.a('null');

        // handle response
        expect(response.statusCode).to.equal(200);

        data = JSON.parse(data);
        expect(data["conforms"]).to.equal(false);
        expect(data["simple"].length).to.equal(2);
        cb();
    });
}

describe('Validator', () => {

    describe('Literal RDF validations', () => {
        it('Validate JSON-LD formatted RDF data against turtle formatted SHACL', (done:Done) => {
            validate_files('./public/testdata.jsonld', 'application/ld+json', './public/testshape.ttl', 'text/turtle', done);
        });

        it('Validate turtle formatted RDF data against turtle formatted SHACL', (done:Done) => {
            validate_files('./public/testdata.ttl', 'text/turtle', './public/testshape.ttl', 'text/turtle', done);
        });

        it('Validate JSON-LD formatted RDF data against JSON-LD formatted SHACL', (done:Done) => {
            validate_files('./public/testdata.jsonld', 'application/ld+json', './public/testshape.jsonld', 'application/ld+json', done);
        });

        it('Validate turtle formatted RDF data against JSON-LD formatted SHACL', (done:Done) => {
            validate_files('./public/testdata.ttl', 'text/turtle', './public/testshape.jsonld', 'application/ld+json', done);
        });
    });

    describe('External RDF validations', () => {
        it('Validate external JSON-LD formatted RDF data against turtle formatted SHACL', (done:Done) => {
            validate_remote_data(base + '/static/testdata.jsonld','application/ld+json', './public/testshape.ttl', 'text/turtle', done);
        });

        it('Validate external turtle formatted RDF data against turtle formatted SHACL', (done:Done) => {
            validate_remote_data(base + '/static/testdata.ttl','text/turtle', './public/testshape.ttl', 'text/turtle', done);
        });

        it('Validate external JSON-LD formatted RDF data against JSON-LD formatted SHACL', (done:Done) => {
            validate_remote_data(base + '/static/testdata.jsonld','application/ld+json', './public/testshape.jsonld', 'application/ld+json', done);
        });

        it('Validate external turtle formatted RDF data against JSON-LD formatted SHACL', (done:Done) => {
            validate_remote_data(base + '/static/testdata.ttl','text/turtle', './public/testshape.jsonld', 'application/ld+json', done);
        });
    });

    describe('RDF against external SHACL', ()=>{
        it('Validate JSON-LD formatted RDF data against external turtle formatted SHACL', (done:Done) => {
            validate_remote_shape('./public/testdata.jsonld', 'application/ld+json', base +'/static/testshape.ttl', 'text/turtle', done);
        });

        it('Validate turtle formatted RDF data against external turtle formatted SHACL', (done:Done) => {
            validate_remote_shape('./public/testdata.ttl', 'text/turtle', base + '/static/testshape.ttl', 'text/turtle', done);
        });

        it('Validate JSON-LD formatted RDF data against external JSON-LD formatted SHACL', (done:Done) => {
            validate_remote_shape('./public/testdata.jsonld', 'application/ld+json', base + '/static/testshape.jsonld', 'application/ld+json', done)
        });

        it('Validate turtle formatted RDF data against external JSON-LD formatted SHACL', (done:Done) => {
            validate_remote_shape('./public/testdata.ttl', 'text/turtle', base + '/static/testshape.jsonld', 'application/ld+json', done)
        });
    });

    describe('External RDF and external SHACL', () => {
        it('Validate external JSON-LD formatted RDF data against external turtle formatted SHACL', (done:Done) => {
            validate_remote(base + '/static/testdata.jsonld', 'application/ld+json', base + '/static/testshape.ttl', 'text/turtle', done);
        });

        it('Validate external turtle formatted RDF data against external turtle formatted SHACL', (done:Done) => {
            validate_remote(base + '/static/testdata.ttl', 'text/turtle', base + '/static/testshape.ttl', 'text/turtle', done);
        });

        it('Validate external JSON-LD formatted RDF data against external JSON-LD formatted SHACL', (done:Done) => {
            validate_remote(base + '/static/testdata.jsonld', 'application/ld+json', base + '/static/testshape.jsonld', 'application/ld+json', done)
        });

        it('Validate external turtle formatted RDF data against external JSON-LD formatted SHACL', (done:Done) => {
            validate_remote(base + '/static/testdata.ttl', 'text/turtle', base + '/static/testshape.jsonld', 'application/ld+json', done)
        });
    });
});
