// Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
module.exports = function (grunt) {
    "use strict";

    grunt.initConfig({
        ts: {
            build: {
                src: ["./src/\*\*/\*.ts"],
                dest: "./dist",
                options: {
                    fast: 'never'
                },
                tsconfig: "./src/tsconfig.json"
            },
            build_test: {
                src: ["./src_test/\*\*/\*.ts"],
                dest: "./test",
                options: {
                    fast: 'never'
                },
                tsconfig: "./src_test/tsconfig.json"
            }
        },
        watch: {
            scripts: {
                files: ["src/\*\*/\*.ts"],
                tasks: ["ts:build"],
                options: {
                    spawn: false
                }
            }
        },
        nodemon: {
            dev: {
                script: 'dist/www.js'
            },
            options: {
                ignore: ['node_modules/**', 'test/**', 'gruntfile.js']
            }
        },
        concurrent: {
            watchers: {
                tasks: ['nodemon', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        mochaTest: {
            test: {
                options: {
                    timeout: 5000
                },
                src: ['test/*.js']
            }
        },
        run: {
            express: {
                cmd: 'node',
                args: [
                    'dist/www.js'
                ]
            },
            wait: {
                cmd: 'node',
                args: [
                    'dist/wait.js'
                ]
            }
        }
    });

    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-nodemon");
    grunt.loadNpmTasks("grunt-concurrent");
    grunt.loadNpmTasks("grunt-mocha-test");
    grunt.loadNpmTasks("grunt-run");

    grunt.registerTask("build", ["ts:build"]);
    grunt.registerTask("serve-dev", ["ts:build", "concurrent:watchers"]);
    grunt.registerTask("serve", ["run:express"]);
    grunt.registerTask("wait", ["run:wait"]);
    grunt.registerTask("test", ["ts:build_test", "mochaTest:test"]);
};