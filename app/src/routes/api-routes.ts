// Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import { Router, Request, Response } from "express";
import * as request from 'request';
import { ValidationReport } from '../types/validation-report';
import { ValidationResponse } from '../types/validation-response';

let SHACLValidator = require('shacl');

export class ApiRoutes {
    private router:Router;

    constructor(router:Router){
        this.router = router;
    }

    public create():void {
        console.log("[ApiRoutes.create] Creating API routes");

        this.router.get('/api', (req: Request, res: Response) => {
            res.send(true);
        });

        this.router.post('/api/validate',(req: Request, res: Response) => {
            if(
                typeof req.body['data'] == 'undefined' ||
                typeof req.body['data_type'] == 'undefined' ||
                typeof req.body['shape'] == 'undefined' ||
                typeof req.body['shape_type'] == 'undefined'
            ) {
                console.log("Invalid parameters");
                res.status(400).send("please provide data, data_type, shape and shape_type");
            } else {
                this.getData(req.body['data'])
                    .then((data:string) => {
                        this.getData(req.body['shape'])
                            .then((shape:string) => {
                                let validator = new SHACLValidator();

                                // to stringify or not to stringify... that is the question
                                data = req.body['data_type'] == 'application/ld+json' && typeof data == 'object' ? JSON.stringify(data) : data;

                                // validate
                                validator.validate( data,  req.body['data_type'], shape, req.body['shape_type'], function (e, report) {
                                    if (e !== null) {
                                        res.status(500).send("Error validating data");
                                    } else {
                                        let response: ValidationResponse = new ValidationResponse();
                                        //console.log("Conforms? " + report.conforms());
                                        response.conforms = report.conforms();
                                        response.jsonld = report;
                                        response.simple = [];

                                        if (response.conforms === false) {
                                            console.log("Number of issues found:"+report.results().length);
                                            let results = report.results();

                                            for(let i = 0; i < results.length; i++) {
                                                response.simple.push(<ValidationReport>({
                                                    node: results[i].focusNode(),
                                                    severity: results[i].severity(),
                                                    path: results[i].path(),
                                                    constraint: results[i].sourceConstraintComponent(),
                                                    message: results[i].message()
                                                }));
                                            }

                                        } else {
                                            console.log("Data conforms to shape");
                                        }

                                        // send response
                                        res.send(response);
                                    }
                                });
                            })
                            .catch(() => {
                                res.status(500).send("Could not retrieve data");
                            });
                    })
                    .catch(() =>{
                        res.status(500).send("Could not retrieve data");
                    });
            }
        });
    }

    private getData(source:string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            let url_regex:RegExp = new RegExp('^http(s)?\:\/\/.+$');
            if(url_regex.test(source)){
                // get url data
                request(source, (error: any, response: request.Response, data: any) => {
                    if(error !== null) {
                        // reject promise
                        console.log(error);
                        reject(error);
                    } else {
                        // handle response
                        if( response.statusCode == 200 ) {
                            console.log("Resolved request");
                            resolve(data);
                        } else {
                            console.log("Unable to retrieve file");
                            reject("Unable to retrieve file");
                        }
                    }
                });
            } else {
                // source IS data
                console.log("Source IS data");
                resolve(source);
            }
        });
    }
}