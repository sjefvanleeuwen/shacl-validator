// Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
export class ValidationReport {
    node: string;
    severity: string;
    path: string;
    constraint: string;
    message: string
}