// Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import {ValidationReport} from "./validation-report";

export class ValidationResponse {
    jsonld: object;
    conforms: boolean;
    simple: ValidationReport[]
}
