// Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import * as request from 'request';

console.log("Wait for host to be up...");

let url = "http://localhost:8080/api";

function wait(){
    request.get(url, (error: any, response: request.Response, data: any) => {
        if((response && response.statusCode == 200)) {
            console.log("Validator is up!")
        } else {
            wait();
        }
    });
}

wait();