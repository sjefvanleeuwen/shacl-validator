# SHACL Validator API

This is an API exposing SHACL validation functionality.

The validator module is used by the Semantic Ledger Platform to validate incoming and outgoing RDF data.
The validity of the data is checked according to a specification in SHACL, the [Shapes Constraint Language](https://shacl.org/).

Given a base URL where the validator is running, and a port number, the SL Platform accesses the validator at `<base_url>:<port>/api/validate`. It is this string that should be set as the value of the VALIDATE_URL environment variable for the SL platform.

The API was authored by Jacco Spek. The SHACL validation is a third party dependency called [shacl-js](https://github.com/TopQuadrant/shacl-js), licensed under Apache 2.0. The present repository is also licensed under Apache 2.0, copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research.

## Requirements
The package was tested on Ubuntu 16.04 LTS
* Docker (v17.05+)

## Building
Build the Docker image. From the root directory:
```shell script
docker build -t sem-ledger/validator .
```

## Running
Start a new docker container (specify desired port to expose):
```shell script
docker run -d -p 8080:8080 --name validator sem-ledger/validator npm run serve
```

## A note on test files
Automated testing is **not supported**. Some test files are included in this repository for inspiration, but there is no guarantee that testing will execute properly.