FROM node:carbon
LABEL "author"="Jacco Spek"
COPY app /usr/src/app
WORKDIR /usr/src/app
RUN npm install && npm run build
